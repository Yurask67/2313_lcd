#include <avr/io.h>
#include "UART.h"

#include "util/delay.h"
#include "avr/interrupt.h"
#include "stdio.h"


#define BAUD 38400L

//#define valbit(reg, bit) ((reg) &   (1 << (bit)))

// void USART_Init( unsigned int baudrate );
// unsigned char USART_Receive( void );
// void USART_Transmit( unsigned char data );
 
// int main( void ) //Главная функция
// {
// 	USART_Init( 47 ); //Скорость соединения 19200 бит/с
// 	for(;;) 	    //Вечный цикл
// 	{
// 		USART_Transmit( USART_Receive() ); //Отправка принятого символа назад
// 	}
// }
 
 
void UART_Init(void) //Функция инициализации USART
{
	UCR = ( ( 1 << RXEN ) | ( 1 << TXEN ) ); //Разрешение на прием и н апередачу через USART и запрет всех прерываний
	UBRR = F_CPU/(16*BAUD) - 1;//уст скорости
    //USR  = 0b00000000; // clear interrupt flags ???
    //DDRD  = 0b00000001; // set Rx (PD0) as input, Tx (PD1) as output?? наоборот???
    _SFR_IO8(DDRD) &= ~(1<<PD0);//вход RX
    _SFR_IO8(DDRD) |= (1<<PD1);//выход TX
    _SFR_IO8(PORTD) |= ~(1<<PD1);//FIXME выход TX в 0 нужно ли?
    //PORTD = 0b00000000; // clear values
}
 
 
/*unsigned char UART_Receive( void ) //Функция приема данных
{
	while ( !(USR & (1<<RXC)) ); 	//Ожидание приема символа
	return UDR; //Возврат символа
}
 
void UART_Transmit( unsigned char data ) //Функция отправки данных
{
	while ( !(USR & (1<<UDRE)) ); //Ожидание опустошения буфера приема
	UDR = data; //Начало передачи данных			        
}
*/



// void uart_getch(void)
// {
//     unsigned char s_char = UDR; // get character
//     uart_putch(s_char); // echo char
//     while(!valbit(USR,RXC)); // wait until receive register is empty
// }

// void uart_putch(const char s_char)
// {
//     while(!valbit(USR,UDRE)); // wait until data register is empty
//     UDR = s_char; // load data register
    
// }
int uart_putchar(char c, FILE *stream);
FILE mystdout = FDEV_SETUP_STREAM(uart_putchar, NULL, _FDEV_SETUP_WRITE);
 

 int uart_putchar(char c, FILE *stream)
{
   if (c == '\n')
      uart_putchar('\r', stream);
   while ( !(USR & (1<<UDRE)) ); //Ожидание опустошения буфера приема
   UDR = c;
   return 0;
}