#include <avr/io.h>
#include <util/delay.h>
#include "ports.h"
#include "lcd1602.h"
#include "constants.h"


 
#define INT8U unsigned char
 
// Определяем порт, соответствующий LCD1602, который удобен для дальнейшего использования.




 using LCD_RS = Port<uint8_t, constants::PIND_, constants::DDRD_, constants::PORTD_, PD2>;
 using LCD_RW = Port<uint8_t, constants::PIND_, constants::DDRD_, constants::PORTD_, PD3>;
 using LCD_E =  Port<uint8_t, constants::PIND_, constants::DDRD_, constants::PORTD_, PD4>;
 using LCD_D4 = Port<uint8_t, constants::PINB_, constants::DDRB_, constants::PORTB_, PB0>;
 using LCD_D5 = Port<uint8_t, constants::PINB_, constants::DDRB_, constants::PORTB_, PB1>;
 using LCD_D6 = Port<uint8_t, constants::PINB_, constants::DDRB_, constants::PORTB_, PB2>;
 using LCD_D7 = Port<uint8_t, constants::PINB_, constants::DDRB_, constants::PORTB_, PB3>;


const bool cmd = 0;
const bool dat = 1;







// #define LCD1602_CTL_DDR		DDRD
// #define LCD1602_CTL_PORT	PORTD
// #define LCD1602_PIN		    PINB
// #define LCD1602_DDR		    DDRB
// #define LCD1602_PORT		PORTB
 
// #define	RS_1()	LCD1602_CTL_PORT |= _BV(PD2)
// #define RS_0()	LCD1602_CTL_PORT &= ~_BV(PD2)
// #define RW_1()	LCD1602_CTL_PORT |= _BV(PD3)
// #define RW_0()	LCD1602_CTL_PORT &= ~_BV(PD3)
// #define E_1()	LCD1602_CTL_PORT |= _BV(PD4)
// #define E_0()	LCD1602_CTL_PORT &= ~_BV(PD4)
// #define LCD1602_CTL_DDR_SET_OUT()	LCD1602_CTL_DDR |= (_BV(PD2) | _BV(PD3) | _BV(PD4))
 
 // обнаружение занятости
// void	Busy_Check()
// {
// 	 LCD1602_DDR = 0x00; // Установить порт в качестве входа
// 	RS_0();	RW_1();
// 	E_1();	_delay_us(20);
// 	while ((LCD1602_PIN & 0X80) == 1); // Старший бит 1 означает занят;
// 	// LCD1602_DDR = 0XFF; // Установить порт обратно в состояние вывода
	
// }
 
 // Записать команду в lcd1602
// void	Write_Command(INT8U CMD)
// {
// 	//Busy_Check();   
// 	LCD1602_DDR = 0XFF;
// 	RS_0();	RW_0();
// 	LCD1602_PORT = CMD;
// 	E_1();E_0(); 
// 	 LCD1602_DDR = 0x00;
// 	_delay_us(50);
// }
 
 // записать байт в lcd1602
// void  Write_Data(INT8U DAT)
// {	
// 	//Busy_Check();
// 	LCD1602_DDR = 0XFF;
// 	 RS_1(); RW_0();
// 	LCD1602_PORT = DAT;
// 	E_1(); E_0(); 
// 	 LCD1602_DDR = 0x00;
// 	 _delay_us(50);
// }
 
 

 // Инициализировать lcd1602, вызвать эту функцию непосредственно в main.c, чтобы инициализировать lcd1602;
// void	Init_LCD1602()
// {
// 	LCD1602_DDR = 0XFF;	 LCD1602_PORT = 0X00;
// 	//LCD1602_CTL_DDR |= 0X0F;
// 	LCD1602_CTL_DDR_SET_OUT();
// 	LCD1602_CTL_PORT = 0X00;
	
// 	//Write_Command(0x38);// 8 бит данные    
// 	Write_Command(0x28);// 4 бит данные    
// 	_delay_ms(15);
// 	Write_Command(0x01);
// 	_delay_ms(15);
// 	Write_Command(0x06);
// 	_delay_ms(15);
// 	Write_Command(0x0c);
// 	_delay_ms(15);
// }


void Write_lcd(INT8U CMD_DAT, INT8U DATA)
{
	//Busy_Check();   
	LCD_D4::output();LCD_D5::output();LCD_D6::output();LCD_D7::output();
	LCD_RS::output();LCD_E::output(); LCD_RW::output();
	
	
	CMD_DAT? LCD_RS::set() : LCD_RS::clear();

	LCD_E::set();
	LCD_D4::write(DATA>>4); LCD_D5::write(DATA>>5); LCD_D6::write(DATA>>6); LCD_D7::write(DATA>>7);
	_delay_us(2);
	 LCD_E::clear();
	_delay_us(50);
	LCD_E::set();
	LCD_D4::write(DATA); LCD_D5::write(DATA>>1); LCD_D6::write(DATA>>2); LCD_D7::write(DATA>>3);
	_delay_us(2);
	LCD_E::clear();
	//LCD_D4::highImpedance();LCD_D5::highImpedance();LCD_D6::highImpedance();LCD_D7::highImpedance();
	
	_delay_us(50);
}

// записать байт в lcd1602
// void  Write_Data(INT8U DAT)
// {	
// 	LCD_D4::output();LCD_D5::output();LCD_D6::output();LCD_D7::output();
// 	LCD_RS::output();LCD_E::output(); LCD_RW::output();
	
// 	LCD_RS::set();
// 	LCD_E::set();	
// 	LCD_D4::write(DAT>>4); LCD_D5::write(DAT>>5); LCD_D6::write(DAT>>6); LCD_D7::write(DAT>>7);
// 	_delay_us(2);
// 	LCD_E::clear();
// 	_delay_us(10);
// 	LCD_E::set();
// 	LCD_D4::write(DAT); LCD_D5::write(DAT>>1); LCD_D6::write(DAT>>2); LCD_D7::write(DAT>>3);
// 	_delay_us(2);
// 	LCD_E::clear();
	
// 	//LCD_D4::highImpedance();LCD_D5::highImpedance();LCD_D6::highImpedance();LCD_D7::highImpedance();
	
// 	_delay_us(50);
// }

// void Write_Init(INT8U CMD)
// {
// 	//Busy_Check();   
// 	LCD_D4::output();LCD_D5::output();LCD_D6::output();LCD_D7::output();
// 	LCD_RS::output();LCD_E::output(); LCD_RW::output();
	
// 	LCD_RS::clear();LCD_RW::clear();
// 	LCD_E::set();
// 	LCD_D4::write(CMD); LCD_D5::write(CMD>>1); LCD_D6::write(CMD>>2); LCD_D7::write(CMD>>3);
// 	_delay_us(2);
// 	LCD_E::clear();
// 	LCD_D4::highImpedance();LCD_D5::highImpedance();LCD_D6::highImpedance();LCD_D7::highImpedance();
		
// }
//В позиции x, строка y, пишем строку str;
void Display_String(INT8U x, INT8U y, char *Str)
{
	INT8U i;
	if (y == 0)
	{
		Write_lcd(cmd, 0x80 | x);
	}
	else if (y == 1)
	{
		Write_lcd(cmd, 0xC0 | x);
	}
	for (i = 0; i < 16 && Str[i] != '\0'; i++)
	{
	Write_lcd(dat, Str[i]);
	}
}

void Init_LCD1602(void)
{
	// LCD1602_DDR = 0XFF;	 LCD1602_PORT = 0X00;
	// LCD1602_CTL_DDR_SET_OUT();
	// LCD1602_CTL_PORT = 0X00;
	
	//Write_Command(0x38);// 8 бит данные    
	
	
	// Write_Init(0x03);
	// _delay_us(50);
	// Write_Init(0x03);
	// _delay_us(50);
	// Write_Init(0x03);
	// _delay_us(50);
	// Write_Init(0x02);// 4 бит данные    
	// _delay_us(50);
	
	Write_lcd(cmd,0x33);
	_delay_us(50);
	Write_lcd(cmd,0x32);
	_delay_us(50);
	Write_lcd(cmd,0x28);
	_delay_us(50);
	Write_lcd(cmd,0x01);
	_delay_ms(15);
	Write_lcd(cmd,0x06);
	_delay_ms(15);
	Write_lcd(cmd,0x0c);
	_delay_ms(15);
}




//----------------------------------------------------------------------------




