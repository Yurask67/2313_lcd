/*

Copyright 2021 Marc Ketel
SPDX-License-Identifier: Apache-2.0

*/

#include "main.h"
#include <avr/interrupt.h>
//#include "io.h"

#include <avr/wdt.h>
#include <util/delay.h>
#include "mcu.h"
#include "timer_isr.h"
#include "stdio.h"
#include "stdlib.h"
#include "lcd1602.h"
#include "wire1.h"
#include "ports.h"
#include "constants.h"



extern "C" {
#include "UART.h"
};

   using Led1= Port<uint8_t, constants::PIND_, constants::DDRD_, constants::PORTD_, PD5>;
   using Led2 = Port<uint8_t, constants::PIND_, constants::DDRD_, constants::PORTD_, PD6>;
 

void setup(void)
{
    wdt_enable(WDTO_2S);
    //wdt_disable();
 
          
    timer_ini();
    Init_LCD1602();
    _delay_us(580);
    port_w1_init();
   
    // Enable interrupts
    sei();
}


int main(void)
{
    setup();

    Timer_soft tims1(5 /*кол-во тиков*/);     // 100 гц
    Timer_soft tims2(1000 /*кол-во тиков*/);    // 0.5 гц
    Timer_soft tim_lcd(500 /*кол-во тиков*/); // 1 гц
    tims1.timer_start();
    tims2.timer_start();
    tim_lcd.timer_start();

    
     UART_Init();                   //
     //stdout = &mystdout;            //перенаправление вывода в UART
   
    Display_String(0,0,"POWER: 32.25 dB");
	Display_String(0,1,"OR    1.007 W");
     

    int16_t static cnt = 0;
    
    Led1::output(); Led2::output();
 //using PortB5 = Port<uint8_t, constants::PINB_, constants::DDRB_, constants::PORTB_, PB5>;

 //PortB5::output();

     for (;;)
    {
         wdt_reset();
        if (tims1.timer_expired())
        {
            tims1.timer_start(); //повторить срабатывание с прежним интервалом
                                 //без учета задержки при обработке

        }

        if (tims2.timer_expired())
        {
            tims2.timer_start(); //повторить срабатывание с прежним интервалом
                                 //без учета задержки при обработке
            // BlinkLed<PortB5>();  // абстрагируя от этого выполняемые задачи
            // PortB5::toggle();
        }

        if (tim_lcd.timer_expired())
        {
            tim_lcd.timer_start(); //повторить срабатывание с прежним интервалом
            Led1::toggle();
            char s1[5];
            itoa(cnt, s1, 10);
            cli();
            Display_String(0, 0, "CNT=");
            Display_String(5, 0, s1);
            sei();
            cnt++;
        }
    }
}

//колбек прерывания таймер0 переполнение
ISR(MCU_TIMER0_OVF_vect)
{
    const uint16_t clkdiv = 1024;
    const uint16_t timerHz = 500;
    TCNT0 = 256 - (F_CPU / clkdiv / timerHz);

    Led2::toggle(); //инверсия
    
    Timer_Cnt_Decr();      //глобальный счетчик --
}
 

 