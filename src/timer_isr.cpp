//#include "io.h"
#include "timer_isr.h"
#include "mcu.h"
//  #include "inttypes.h"
#include "avr/interrupt.h"
#include "avr/io2313.h"
#include "main.h"

void timer_ini(void)
{
  //настройка таймеров

  // TCCR1B |= (1<<CTC1); // устанавливаем режим СТС (сброс по совпадению)
  // TIMSK |= (1<<OCIE1A); //устанавливаем бит разрешения прерывания 1ого счетчика по совпадению с OCR1A(H и L)
  
    //TCCR0 &= 0b0000011; //СК/64 делитель тактовой частоты для таймера 0
  //MCU_TCCR0 |= _BV(CS00) | _BV(CS01); // clk/64
  MCU_TCCR0 |= _BV(CS02) | _BV(CS00); // clk/1024

  //настройка и управление прерываниями

  // Бит 7 - I: Общее разрешение прерываний. Для разрешения прерываний этот бит должен быть
  // установлен в единицу. Управление отдельными прерываниями производится регистром маски
  // прерываний - GIMSK/TIMSK. Если флаг сброшен (0), независимо от состояния GIMSK/TIMSK,
  // прерывания не разрешены. Бит I очищается аппаратно после входа в прерывание и вос-
  // станавливается командой RETI, для разрешения обработки следующих прерываний.

  // SREG |= 1<<SREG_I; //разрешение глобального прерывания sei();
  // SREG &= 0x7F; //запрещение глобального прерывания  cli();
  
  //TIMSK |= 1 << TOIE0; //Разрешение прерывания по переполнению таймера/счетчика 0.
  TIMSK |= _BV(TOIE0);
}




// софт таймер http://we.easyelectronics.ru/Soft/samyy-prostoy-programmnyy-taymer.html
void Timer_soft::timer_start()
{
  Timer_struct.start = get_time();
  Timer_struct.flag = started;
}

// void Timer_soft::timer_reset()
// {
//   Timer_struct.start += Timer_struct.interval;
//   Timer_struct.flag = started;
// }

bool Timer_soft::timer_expired()
{
  if (Timer_struct.flag)
  {
    return started;
  }
  if ((Timer_struct.start + Timer_struct.interval) <= get_time())
  {
    Timer_struct.flag = stopped;
    return stopped;
  }
  return started;
}

/***********************/
static time_t Timer_Incr; //глобальный счетчик прерываний таймера

time_t get_time(void)
{return Timer_Incr;}

void Timer_Cnt_Decr(void)
{Timer_Incr++;}
/*---------------------*/
