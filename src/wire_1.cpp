#include "wire1.h"
//#include "main.h"
//#include <stdbool.h>
#include "util/delay.h"
#include "ports.h"
#include "constants.h"

#define delay_us(u) _delay_us(u) //???
#define delay_ms(m) _delay_ms(m) //???

using Pin_W1 = Port<uint8_t, constants::PINB_, constants::DDRB_, constants::PORTB_, PB7>;

// #define	Pin_W1_1()		Port_W1 |= _BV(Pin_W1)
// #define Pin_W1_0()		Port_W1 &= ~_BV(Pin_W1)
// #define DDR_W1_OUT()	DDR_W1 |= _BV(Pin_W1)
// #define DDR_W1_IN()		DDR_W1 &= ~_BV(Pin_W1)
// #define Pin_W1_Read()	Pin_W1_PIN & _BV(Pin_W1)

// #define E_1()	LCD1602_CTL_PORT |= _BV(PD4)
// #define E_0()	LCD1602_CTL_PORT &= ~_BV(PD4)



//-------------------------------------------------

//-------------------------------------------------
uint8_t Wire1_ReadBit(void) {
	uint8_t bit = 0;
	//HAL_GPIO_WritePin(Port_W1, Pin_W1,0); 
	Pin_W1::output(); Pin_W1::clear();
	delay_us(2);
	//HAL_GPIO_WritePin(Port_W1, Pin_W1, 1); 
	Pin_W1::set();
	delay_us(13);
	Pin_W1::pullUp(); bit = Pin_W1::read(); 
	delay_us(45);
	return bit;
}
//-----------------------------------------------

//-----------------------------------------------
uint8_t Wire1_ReadByte(void) {
	uint8_t data = 0;

	for (uint8_t i = 0; i <= 7; i++)
		{
			data += Wire1_ReadBit() << i;
		}

	return data;
}
//-------------------------------------------------

//-----------------------------------------------
void Wire1_WriteBit(uint8_t bit) {
	//HAL_GPIO_WritePin(Port_W1, Pin_W1, 0);
	Pin_W1::output(); Pin_W1::clear();
	delay_us(bit ? 3 : 65);
	//HAL_GPIO_WritePin(Port_W1, Pin_W1, 1);
	Pin_W1::set();
	delay_us(bit ? 65 : 3);
}
//-----------------------------------------------

//-----------------------------------------------
void Wire1_WriteByte(uint8_t dt) {
	for (uint8_t i = 0; i < 8; i++)
		{
			Wire1_WriteBit(dt >> i & 1);
			//Delay Protection
			delay_us(5);
		}
}
//-----------------------------------------------

//--------------------------------------------------
void port_w1_init(void)  

{
	//http://narodstream.ru/stm-urok-92-datchik-temperatury-ds18b20-chast-1/
	// HAL_GPIO_DeInit(Port_W1, Pin_W1);
	// GPIO_InitTypeDef GPIO_InitStruct = { 0 };
	// GPIO_InitStruct.Pin = Pin_W1;
	// GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_OD;
	// GPIO_InitStruct.Pull = GPIO_NOPULL;
	// GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	// HAL_GPIO_Init(Port_W1, &GPIO_InitStruct);

	// //����� ������������ ������ 1 wire
	// Port_W1->LCKR |= (0x01 << Pin_W1);  ///�������� ����� ������� ����� ��������
	// Port_W1->LCKR |= GPIO_LCKR_LCKK;  //���������� 1
	// Port_W1->LCKR &= ~GPIO_LCKR_LCKK;  //���������� 0
	// Port_W1->LCKR |= GPIO_LCKR_LCKK;  //���������� 1
	// uint16_t temp;
	// temp = Port_W1->LCKR;  //��������� 2 ����
	// temp = Port_W1->LCKR;
Pin_W1::pullUp();	
}
//--------------------------------------------------

//--------------------------------------------------
uint8_t Wire1_Reset(void) {
	uint16_t status;
	//Wire1_GPIO_Port->ODR &= ~GPIO_ODR_ODR10;//
	//HAL_GPIO_WritePin(Port_W1, Pin_W1, 0); //низкий уровень
	Pin_W1::output(); Pin_W1::clear();
	delay_us(485);  ////задержка как минимум на 480 микросекунд
	//Wire1_GPIO_Port->ODR |= GPIO_ODR_ODR10;//высокий уровен
	//HAL_GPIO_WritePin(Port_W1, Pin_W1, 1);
	Pin_W1::set();
	delay_us(65);    //задержка как минимум на 60 микросекунд
	//status = Port_W1->IDR & Pin_W1;    //проверяем уровень
	Pin_W1::pullUp(); status = Pin_W1::read(); 
	delay_us(500);    //задержка как минимум на 480 микросекунд
	//(на всякий случай подождём побольше, так как могут быть неточности в задержке)
	return (status ? 1 : 0);    //вернём результат
}
//--------------------------------------------------

//--------------------------------------------------
//Мы будем использовать режим SKIP ROM.  ������� �������������

uint8_t ds18b20_init(uint8_t mode) {
	if (Wire1_Reset())
		{
			return 1;
		}

	if (mode == SKIP_ROM)
		{
			//SKIP ROM
			Wire1_WriteByte(0xCC);
			//WRITE SCRATCHPAD
			Wire1_WriteByte(0x4E);
			//TH REGISTER 100 ��������
			Wire1_WriteByte(0x64);
			//TL REGISTER - 30 ��������
			Wire1_WriteByte(0x9E);
			//Resolution 12 bit
			Wire1_WriteByte(RESOLUTION_12BIT);

			//������� ������ � EEPROM - ����� + ����� �������� ��� ������� + 0x48
				{
					//    Wire1_Reset(); // ����� ����� ������� � EEPROM
					//   Wire1_WriteByte(0xCC);
					//   Wire1_WriteByte(0x48);
					//��� ���� ������� ��� ���������� ���������������
					//��������� ������� �� ����. �������, ����� ������ ������� ����������� ���������� � EEPROM
				}
		}

	return 0;
}

//--------------------------------------------------
void ds18b20_MeasureTemperCmd(uint8_t mode, uint8_t DevNum)

{
	Wire1_Reset();

	if (mode == SKIP_ROM)
		{
			//SKIP ROM
			Wire1_WriteByte(0xCC);
		}

	//CONVERT T
	Wire1_WriteByte(0x44);
}
//----------------------------------------------------------

//----------------------------------------------------------
void ds18b20_ReadStratcpad(uint8_t mode, uint8_t *Data, uint8_t DevNum) {
	uint8_t i;
	Wire1_Reset();

	if (mode == SKIP_ROM)
		{
			//SKIP ROM
			Wire1_WriteByte(0xCC);
		}

	//READ SCRATCHPAD
	Wire1_WriteByte(0xBE);

	for (i = 0; i < 8; i++)
		{
			Data[i] = Wire1_ReadByte();
		}
}
//----------------------------------------------------------

//----------------------------------------------------------

//----------------------------------------------------------

//--------------------------------------------------
uint8_t ds1995_init(uint8_t mode) {
	if (Wire1_Reset())
		{
			return 1;
		}

	if (mode == SKIP_ROM)
		{
			//SKIP ROM
			Wire1_WriteByte(0xCC);

		}

	return 0;
}
//--------------------------------------------------

//----------------------------------------------------------

#define CRC8INIT    0x00
#define CRC8POLY    0x18              //    X^8+X^5+X^4+X^0
//    0x18 = ( 0x130 >> 1 ) & 0x7F
uint8_t crc8_8540(uint8_t *data_in, uint8_t bytes) {
	uint8_t crc = CRC8INIT;
	uint8_t bit_cnt;
	register uint8_t data;
	register uint8_t feedback_bit;

	while (bytes--)
		{
			data = *data_in++;

			for (bit_cnt = 8; bit_cnt; bit_cnt--)
				{
					feedback_bit = (data ^ crc) & 0x01;
					crc >>= 1;
					data >>= 1;

					if (feedback_bit)
						{
							crc ^= ((CRC8POLY >> 1) | 0x80);
						}
				}
		}

	return (crc);
}
//----------------------------------------------------------

//----------------------------------------------------------
void ds1995_ReadStratcpad(uint8_t *Data, uint8_t DevNum) {
	uint8_t i;
	Wire1_Reset();
	//READ SCRATCHPAD
	Wire1_WriteByte(0x33);

	for (i = 0; i < 8; i++)
		{
			Data[i] = Wire1_ReadByte();
		}
}
//----------------------------------------------------------

//----------------------------------------------------------
void ds1995_ReadMem(uint8_t mode, uint8_t *Data, uint16_t adr_bytes, uint16_t x_bytes) {
	uint16_t i;
	Wire1_Reset();

	if (mode == SKIP_ROM)
		{
			//SKIP ROM
			Wire1_WriteByte(0xCC);
		}

	Wire1_WriteByte(0xF0); //������ memory
	Wire1_WriteByte(adr_bytes & 0x00FF); //������� ���� ������
	Wire1_WriteByte(adr_bytes >> 8 & 0x00FF); // ������� ���� ������

	for (i = 0; i < x_bytes; i++)
		{
			Data[i] = Wire1_ReadByte();
			//printf(" %02X", Data[i]);
		}
}
//----------------------------------------------------------

// ----------------------------------------------------------
uint8_t ds1995_WrBytes(uint8_t mode, uint8_t Data_w, uint16_t adr_bytes) {
	uint8_t scrath[3];
	uint16_t i, Data_r;
	Wire1_Reset();

	if (mode == SKIP_ROM)
		{
			//SKIP ROM
			Wire1_WriteByte(0xCC);
		}

	Wire1_WriteByte(0x0F); //������ ������
	Wire1_WriteByte(adr_bytes & 0x00FF); //������� ���� ������
	Wire1_WriteByte(adr_bytes >> 8 & 0x00FF); // ������� ���� ������
	Wire1_WriteByte(Data_w);
	Wire1_Reset();

	if (mode == SKIP_ROM)
		{
			//SKIP ROM
			Wire1_WriteByte(0xCC);
		}

	Wire1_WriteByte(0xAA); //Issue read scratchpad command

	for (i = 0; i < 3; i++)
		{
			scrath[i] = Wire1_ReadByte();
		}

	Data_r = Wire1_ReadByte();

	if (Data_r != Data_w)
		{
			return 0;
		} //Read TA1, Read TA1, Read E/S

	Wire1_Reset();

	if (mode == SKIP_ROM)
		{
			//SKIP ROM
			Wire1_WriteByte(0xCC);
		}

	Wire1_WriteByte(0x55); //Issue copy scratchpad command

	for (i = 0; i < 3; i++)
		{
			Wire1_WriteByte(scrath[i]); //Write TA1, Write TA1, Write E/S - AUTHORIZATION CODE
		}

	Wire1_Reset();
	return 1;
}
//----------------------------------------------------------

void ds1995_Clear(void) {
	uint8_t status = ds1995_init(SKIP_ROM); //�������������
	uint16_t adr;

	if (!status)
		{          //� �������� ��� ���� �������
			for (adr = 0; adr < 0x800; adr++)
				{
					while (!ds1995_WrBytes(SKIP_ROM, 0x55, adr))
						; // ������ 55 �� �������� ������ � ����� ���� �� �������
				}
		}
}

//--------------------------------------------------
int16_t call_temper(void) {
	uint8_t dt[8];
	ds18b20_MeasureTemperCmd(SKIP_ROM, 0);
	delay_us(800); //FIXME us vs ms???

	ds18b20_ReadStratcpad(SKIP_ROM, dt, 0);

	return (int16_t) ((dt[1] << 8) | dt[0]);
}

//----------------------------------------------
