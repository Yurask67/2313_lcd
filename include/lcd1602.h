#ifndef LCD1602_H_
#define LCD1602_H_
 
#include "ports.h"
#define INT8U unsigned char
 
 extern void Busy_Check (); // Обнаружение занятости
 extern void Write_Command (INT8U Command); // Написать команду
 extern void Write_Data (INT8U DATA); // Запись данных
 extern void Display_String (INT8U x, INT8U y, char * Str); // Записать строку Str в порядке от бита x строки y
 extern void Init_LCD1602 (); // Инициализация LCD1602
 

 //extern void BlinkLed(void); //XXX



 
#endif //LCD1602_H_