#ifndef WIRE1_H_
#define WIRE1_H_

//--------------------------------------------------

#include <stdint.h>
//#include "io.h"


//--------------------------------------------------

//--------------------------------------------------

#define SKIP_ROM 0
#define NO_SKIP_ROM 1

//--------------------------------------------------

#define RESOLUTION_9BIT 0x1F
#define RESOLUTION_10BIT 0x3F
#define RESOLUTION_11BIT 0x5F
#define RESOLUTION_12BIT 0x7F

// #define Port_W1 PORTD
// #define DDR_W1  DDRD
// #define Pin_W1_PIN  PIND
// #define Pin_W1  PD6

void port_w1_init(void);


//--------------------------------------------------

//--------------------------------------------------
#endif /* WIRE1_H_ */