
#ifndef CONSTANTS_H
#define CONSTANTS_H

#include <avr/io.h>


// определяем собственное пространство имен для хранения констант
namespace constants
{
extern "C" {
const int PORTB_ = (int) &PORTB;
const int  DDRB_ = (int) &DDRB;
const int PINB_  = (int) &PINB;
const int PORTD_ = (int) &PORTD;
const int  DDRD_ = (int) &DDRD;
const int PIND_  = (int) &PIND;
};
}

#endif //CONSTANTS_H