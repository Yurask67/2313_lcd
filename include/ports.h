#ifndef PORTS_H_INCLUDED // если этот файл ещё не включён
#define PORTS_H_INCLUDED // включить этот файл



/*
Представляет порт ввода вывода
*/
template<typename Size, int PinAddress, int DirectionAddress, int PortAddress, int... Bits>
struct Port;

/*
Представляет выбранный бит порта ввода вывода
*/
template<typename Size, int PinAddress, int DirectionAddress, int PortAddress, int Bit>
struct Port<Size, PinAddress, DirectionAddress, PortAddress, Bit> final
{
public:
    // Тип, определяющий размер текущего порта ввода вывода
    //using size = Size;

public:
    // // Адрес регистра порта ввода вывода
    // static constexpr auto address = PortAddress;

    // // Адрес регистра ножек порта ввода вывода
    // static constexpr auto pinAddress = PinAddress;

    // // Адрес регистра направления порта ввода вывода
    // static constexpr auto directionAddress = DirectionAddress;

    // // Адрес регистра порта ввода вывода
    // static constexpr auto portAddress = PortAddress;

    // // Выбранный бит порта ввода вывода
    //static constexpr auto bit = Bit;

private:
    // // Указатель на регистр ножек порта ввода вывода
    // static constexpr auto pinReg = (volatile Size*)PinAddress;

    // // Указатель на регистр направления порта ввода вывода
    // static constexpr auto directionReg = (volatile Size*)DirectionAddress;

    // // Указатель на регистр порта ввода вывода
    // static constexpr auto portReg = (volatile Size*)PortAddress;

    // Маска, соответствующая выбранному биту порта ввода вывода
    static constexpr auto mask = 1 << Bit;

public:
    // Устанавливает выбранный бит порта ввода вывода равным 1
    static void set() {*(volatile Size*)PortAddress |= mask; }

    // Устанавливает выбранный бит порта ввода вывода равным 0
    static void clear() {*(volatile Size*)PortAddress &= ~mask; }

    // Инвертирует выбранный бит порта ввода вывода
    static void toggle() {*(volatile Size*)PortAddress ^= mask; }

    // Выполняет запись младшего бита значения в выбранный бит порта ввода вывода
    static void write(Size value) {*(volatile Size*)PortAddress = (*(volatile Size*)PortAddress & ~mask) | ((value << Bit) & mask); }

    // Выполняет чтение выбранного бита порта ввода вывода в младший бит возвращаемого значения
    static Size read() { return (*(volatile Size*)PinAddress & mask) >> Bit; }

    // Устанавливает режим ввода с подтягивающим резистором для выбранного бита порта ввода вывода
    static void pullUp() {*(volatile Size*)DirectionAddress &= ~mask, *(volatile Size*)PortAddress |= mask; }

    // Устанавливает режим ввода с высокоимпедансным состоянием для выбранного бита порта ввода вывода
    static void highImpedance() {*(volatile Size*)DirectionAddress &= ~mask, *(volatile Size*)PortAddress &= ~mask; }

    // Устанавливает режим вывода для выбранного бита порта ввода вывода
    static void output() {*(volatile Size*)DirectionAddress |= mask; }
};

/*
Представляет все биты порта ввода вывода
*/
template<typename Size, int PinAddress, int DirectionAddress, int PortAddress>
struct Port<Size, PinAddress, DirectionAddress, PortAddress> final
{
public:
    // Тип, определяющий размер текущего порта ввода вывода
    using size = Size;

public:
    // // Адрес регистра порта ввода вывода
    // static constexpr auto address = PortAddress;

    // // Адрес регистра ножек порта ввода вывода
    // static constexpr auto pinAddress = PinAddress;

    // // Адрес регистра направления порта ввода вывода
    // static constexpr auto directionAddress = DirectionAddress;

    // // Адрес регистра порта ввода вывода
    // static constexpr auto portAddress = PortAddress;

private:    
    // // Указатель на регистр ножек порта ввода вывода
    // static constexpr auto pinReg = (volatile Size*)PinAddress;

    // // Указатель на регистр направления порта ввода вывода
    // static constexpr auto directionReg = (volatile Size*)DirectionAddress;

    // // Указатель на регистр порта ввода вывода
    // static constexpr auto portReg = (volatile Size*)PortAddress;

    // Маска, соответствующая всем битам порта ввода вывода
    static constexpr auto mask = ~(Size)0;

public:
    // Устанавливает все биты порта ввода вывода равным 1
    static void set() {*(volatile Size*)PortAddress = mask; }

    // Устанавливает все биты порта ввода вывода равным 0
    static void clear() { *(volatile Size*)PortAddress = ~mask; }

    // Инвертирует все биты порта ввода вывода
    static void toggle() { *(volatile Size*)PortAddress ^= mask; }

    // Выполняет запись значения в порт ввода вывода
    static void write(Size value) { *(volatile Size*)PortAddress = value; }

    // Выполняет чтение порта ввода вывода в возвращаемое значение
    static Size read() { return *(volatile Size*)PortAddress; }

    // Устанавливает режим ввода с подтягивающим резистором для всех битов порта ввода вывода
    static void pullUp() { *(volatile Size*)DirectionAddress = ~mask, *(volatile Size*)PortAddress = mask; }

    // Устанавливает режим ввода с высокоимпедансным состоянием для всех битов порта ввода вывода
    static void highImpedance() { *(volatile Size*)DirectionAddress = ~mask, *(volatile Size*)PortAddress = ~mask; }

    // Устанавливает режим вывода для всех битов порта ввода вывода
    static void output() { *(volatile Size*)DirectionAddress = mask; }
};


template<typename Port>  // Порт передается как шаблонный параметр функции
void BlinkLed(void)      // Выполняемая задача. Не имеет представления, с каким конкретно портом она работает
{                        // Это даже может быть не сам порт, а заглушка для тестирования

    Port::output();      // Настройка порта на вывод. Необходимо, так как задача меняет состояние порта
    Port::toggle();  // Переключение ножки в обратное логическое состояние
}

#endif //PORTS_H_INCLUDED