
#ifndef TIMER_ISR_H_
#define TIMER_ISR_H_

#include <stdio.h>
//для софт таймера http://we.easyelectronics.ru/Soft/samyy-prostoy-programmnyy-taymer.html
typedef uint32_t time_t; //при частоте глобального таймера 1000 гц = 49 суток


#define  started  0
#define  stopped  1
// #define  first    2
  


typedef struct
{
  time_t start;
  uint16_t interval;
  bool flag;
} Timer_t;

/*********************/
class Timer_soft
{
private:
  Timer_t Timer_struct;

public:
  Timer_soft(time_t set_interval) //конструктор
  {
    Timer_struct.interval = set_interval;
  };
  void timer_start();
  // void timer_reset();
  bool timer_expired();
};

/*------------------*/
time_t get_time(void);
void Timer_Cnt_Decr(void);
void timer_ini(void);


#endif //TIMER_ISR_H